package Model;
import java.util.*;

    public abstract class NewYearDecorations {
        String color;
        String shape;
        int cost;

        public NewYearDecorations(String color, String shape, int cost) {
            this.color = color;
            this.shape = shape;
            this.cost = cost;
        }

        public NewYearDecorations() {
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            NewYearDecorations that = (NewYearDecorations) o;
            return cost == that.cost &&
                    Objects.equals(color, that.color) &&
                    Objects.equals(shape, that.shape);
        }

        @Override
        public int hashCode() {
            return Objects.hash(color, shape, cost);
        }

        @Override
        public String toString() {
            return "newYearDecorations{" +
                    "color='" + color + '\'' +
                    ", shape='" + shape + '\'' +
                    ", cost=" + cost +
                    '}';
        }
    }
}
