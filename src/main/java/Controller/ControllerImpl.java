package Controller;

import Model.NewYearStore;
import Model.MyException;

public class ControllerImpl implements Controller {


    public void findDecorationByType() throws MyException {
       NewYearStore.findDecorationByType();
    }


    public void printAll() {
        NewYearStore.printAll();
    }
}
