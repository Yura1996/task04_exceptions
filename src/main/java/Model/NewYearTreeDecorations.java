package Model;
import java.util.ArrayList;
import java.util.List;
public class NewYearTreeDecorations extends NewYearDecorations {
    private static List<NewYearDecorations> list = new ArrayList<>();

    public NewYearTreeDecorations() {
        super();
    }

    public NewYearTreeDecorations(String color, String shape, int cost) {
        super(color, shape, cost);
    }
}


