package Controller;
import Model.MyException;

public interface Controller {

        void findDecorationByType() throws MyException;
        void printAll();
    }

