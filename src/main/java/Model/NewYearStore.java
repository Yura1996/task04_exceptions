package Model;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

    public class NewYearStore {
        public static List<NewYearDecorations> homeList = new ArrayList<>();
        public static List<NewYearDecorations> treeList = new ArrayList<>();

        static{
            treeList.add(new NewYearTreeDecorations("Green","Circle",25));
            homeList.add(new HomeNewYearDecor("Red","Squad",50));
            homeList.add(new HomeNewYearDecor("Pink","Circle",20));
            treeList.add(new NewYearTreeDecorations("Green","Circle",40));
            treeList.add(new NewYearTreeDecorations("White","Stick",210));
        }

        public static void printTreeList() {
            for (NewYearDecorations decorations : treeList) {
                System.out.println(decorations);
            }
        }

        public static void printHomeList() {
            for (NewYearDecorations decorations : homeList) {
                System.out.println(decorations);
            }
        }

        public static void printAll() {
            printHomeList();
            printTreeList();
        }

        public static void findDecorationByType () throws MyException {
            Scanner scr = new Scanner(System.in);
            String decorationType = scr.nextLine();

            if (decorationType.equals("NewYear tree")) {
                printTreeList();
            }else if (decorationType.equals("Home decor")) {
                printHomeList();
            }else {
                throw new MyException("Напишіть NewYear tree або Home decor");
            }


        }
    }


